from typing import List, Optional
from common import PublicGame, Player, LocalGame


class DataController:
    connected_players: List[Player] = []
    available_games: List[PublicGame] = []
    local_game: Optional[LocalGame] = None
