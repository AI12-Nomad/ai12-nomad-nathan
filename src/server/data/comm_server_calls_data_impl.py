from typing import List
from common.data_structures.public_game import PublicGame
from src.common.data_structures.profiles import Player, Profile
from src.server.data.data_server_controller import DataServerController


class CommServerCallsDataImpl:
    def create_game(self, public_game: PublicGame):
        DataServerController.game_list.append(public_game)

    def get_players(self) -> List:
        return DataServerController.player_list

    def get_available_games(self) -> List:
        available_games_list: List[PublicGame] = []
        for games in DataServerController.game_list:
            if games.status == "available":
                available_games_list.append(games)
        return available_games_list

    def add_user(self, profile: Profile) -> Player:
        new_player = Player(profile.nickname, profile.uuid)
        if new_player not in DataServerController.player_list:
            DataServerController.player_list.append(new_player)
        else:
            pass  # See how to handle errors
        return new_player
