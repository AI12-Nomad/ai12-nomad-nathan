from common import I_IHMServerCallsComm
from networking import create_server


class IHMServerCallsComm_Impl(I_IHMServerCallsComm):
    def quit_server(self) -> None:
        pass

    async def start_server(self, server) -> None:
        pass
