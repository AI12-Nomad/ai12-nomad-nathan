from ihm_server_calls_comm_impl import IHMServerCallsComm_Impl
from common import I_IHMServerCallsComm
from typing import Dict
from networking.common import IO
from networking import create_server


class ComController:
    def __init__(self) -> None:
        self.__interface_ihm_server = IHMServerCallsComm_Impl(self)

    ## client map with the create_server IO.client_map
    async def create_server(self, ip: str, port: int) -> None:
        """creating server"""
        pass

    def get_interface_for_ihm_server(self) -> I_IHMServerCallsComm:
        """getter for interface ihm main such as in Java"""
        return self.__interface_ihm_server
