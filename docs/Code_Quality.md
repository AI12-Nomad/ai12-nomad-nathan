# Document pour les bonnes pratiques qualités

## Version minimale

Il faut au moins python 3.7.0.

## Nommage

Langue :

- Le code en anglais
- Les commentaires en anglais

Casse :

- fichiers en `snake_case.py`
- fonctions, variables, attributs en `snake_case`
- classes d'objets en `PascalCase`

```python3
# filename : my_class.py
class MyClass:
    """
    A simple example class.
    """

    def __init__(self, x: int, y: int = 0):
        self.my_x = x
        self.my_y = y


def is_on_origin(x: int, y: int) -> bool:
    return not x and not y


# Implementing MyClass with my_object
my_object = MyClass(0)
```

- Il faut commenter toutes les classes et les fonctions qui ne sont pas triviales.
- On conseille de commenter plus largement le corps des longues fonctions

## Style

Pour être en phase sur le style du code et ne pas faire de guerre d'édition sur l'indentation & les whitespaces, un outil : `black`
Usage : `black file.py` <!-- va formatter de manière déterministe le code. Il est utilisé dans une majorité des projets python. -->

### Dunder methods

Les *dunder methods* devraient être utilisées quand c'est approprié.

```python3
class String:
    def __init__(self, string: str):
        self.string = string

    def __repr__(self):
        return f'Object: {self.string}'

    def __str__(self):
        return self.string
```

## Vérifications de type

`mypy` permet de vérifier la cohérence des types des variables.
Pour un meillleur usage, il faut au minimum donner le type des variables dans la signature des fonctions.

```python3
def greeting(name: str) -> str:
    return 'Hello ' + name

def is_on_origin(x: int, y: int) -> bool:
    return not x and not y
```

## Tests unitaires

Pour les tests unitaires nous utiliserons `unittest`.

```python3
import unittest

class TestStringMethods(unittest.TestCase):
    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

if __name__ == '__main__':
    unittest.main()
```

Exécution :
```
python -m unittest discover
```

## Modules & import

- Tous les fichiers python doivent être dans un `module` python.
- Les `import` ne doivent pas être relatifs.
- Éviter d'importer l'entiéreté d'un `module`.

```python3
from game.logic import calculate_points

...

points: int = calculate_points(player, map)
```
## Gestion des dépendances

On fait un venv avec un requirements.txt
voir la page d'open classrooms
(python3-venv)
